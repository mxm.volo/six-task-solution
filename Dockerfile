FROM maven:3.8.6-openjdk-18-slim

COPY ./ /app/

RUN cd /app/ && \
    mvn clean install -DskipTests

WORKDIR /app/

CMD ["mvn", "verify"]
