@trades
Feature: Fulfilled orders create trades

  Scenario Outline: Buy and sell orders result in successful trade
    Given User <userA> has signed up
    And User <userB> has signed up
    And Security <security> is added to the system
    When User <userA> places <typeA> order for security <security> with price <priceA> and quantity <qtyA>
    And User <userB> places <typeB> order for security <security> with price <priceB> and quantity <qtyB>
    Then Trade should appear with price <tradePrice> and quantity <tradeQty> and orders be fulfilled
    Examples:
      | userA | userB | typeA | typeB | security | priceA | qtyA | priceB | qtyB | tradePrice | tradeQty |
      | John  | Mark  | BUY   | SELL  | AMZN     | 999.99 | 2    | 999.8  | 3    | 999.8      | 2        |
      | Green | Blue  | SELL  | BUY   | ORCL     | 49.00  | 3    | 50.0   | 3    | 49.0       | 3        |

  Scenario: Buy and sell orders result in unsuccessful trade
    Given User John has signed up
    And User Mark has signed up
    And Security MSFT is added to the system
    When User John places BUY order for security MSFT with price 99 and quantity 4
    And User Mark places SELL order for security MSFT with price 100 and quantity 3
    Then Trade should not happen