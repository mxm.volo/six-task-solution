@securities
Feature: Basic securities tests

  Scenario: All added securities can be selected
    Given New securities are added to the system
      | GOOGL |
      | NKE   |
      | AAPL  |
    When List of securities is requested
    Then All added securities are returned