package actions;

import models.orders.OrderModel;
import requests.orders.GetAllOrdersRequest;
import requests.orders.GetOrderRequest;
import requests.orders.PostNewOrderRequest;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class OrdersActions {

    private static ThreadLocal<OrdersActions> instance;

    public static OrdersActions getOrdersActions() {
        if(Objects.isNull(instance)) {
            instance = ThreadLocal.withInitial(OrdersActions::new);
        }
        return instance.get();
    }

    public OrderModel getOrder(UUID orderId) {
        return new GetOrderRequest()
                .setOrderId(orderId)
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public List<OrderModel> getOrders() {
        return new GetAllOrdersRequest()
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public OrderModel placeOrder(OrderModel newOrderData) {
        return new PostNewOrderRequest()
                .setPayload(newOrderData)
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }
}
