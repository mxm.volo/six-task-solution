package actions;

import io.cucumber.java.PendingException;
import models.users.UserModel;
import requests.users.GetAllUsersRequest;
import requests.users.GetUserRequest;
import requests.users.PostNewUserRequest;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class UsersActions {

    private static ThreadLocal<UsersActions> instance;

    public static UsersActions getUsersAction() {
        if(Objects.isNull(instance)) {
            instance = ThreadLocal.withInitial(UsersActions::new);
        }
        return instance.get();
    }

    public UserModel getUser(String userId) {
        return new GetUserRequest()
                .setUserId(UUID.fromString(userId))
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public List<UserModel> getUsers() {
        return new GetAllUsersRequest()
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public UserModel createUser(String username, String password) {
        UserModel newUserData = new UserModel();
        newUserData.setUsername(username);
        newUserData.setPassword(password);

        return new PostNewUserRequest()
                .setPayload(newUserData)
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public UserModel getUserByName(String username) {
        return getUsers().stream()
                .filter(user -> user.getUsername().equals(username))
                .findFirst()
                .orElseThrow(() ->
                        new PendingException("User with name '" + username + "' has not been found!"));
    }
}
