package actions;

import models.trades.TradeModel;
import org.apache.http.HttpStatus;
import requests.trades.GetAllTradesRequest;
import requests.trades.GetSpecificTradeRequest;
import requests.trades.GetTradeRequest;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class TradesActions {

    private static ThreadLocal<TradesActions> instance;

    public static TradesActions getTradesActions() {
        if(Objects.isNull(instance)) {
            instance = ThreadLocal.withInitial(TradesActions::new);
        }
        return instance.get();
    }

    public TradeModel getTrade(String tradeId) {
        return new GetTradeRequest()
                .setTradeId(UUID.fromString(tradeId))
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public TradeModel getTradeByBuyerAndSellerIds(UUID orderBuyerId, UUID orderSellerId) {
        return new GetSpecificTradeRequest()
                .setBuyerAndSellerIds(orderBuyerId, orderSellerId)
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public List<TradeModel> getTrades() {
        return new GetAllTradesRequest()
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public void getFailedTradeByBuyAndSellOrderIds(UUID orderBuyerId, UUID orderSellerId) {
        new GetSpecificTradeRequest()
                .setBuyerAndSellerIds(orderBuyerId, orderSellerId)
                .sendRequest()
                .assertStatusCode(HttpStatus.SC_NOT_FOUND);
    }
}
