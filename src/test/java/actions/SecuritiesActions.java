package actions;

import io.cucumber.java.PendingException;
import models.securities.SecurityModel;
import requests.securities.GetAllSecuritiesRequest;
import requests.securities.GetSecurityRequest;
import requests.securities.PostNewSecurityRequest;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class SecuritiesActions {

    private static ThreadLocal<SecuritiesActions> instance;

    public static SecuritiesActions getSecuritiesActions() {
        if(Objects.isNull(instance)) {
            instance = ThreadLocal.withInitial(SecuritiesActions::new);
        }
        return instance.get();
    }

    public SecurityModel getSecurity(String securityId) {
        return new GetSecurityRequest()
                .setSecurityId(UUID.fromString(securityId))
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public List<SecurityModel> getSecurities() {
        return new GetAllSecuritiesRequest()
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public SecurityModel createSecurity(String name) {
        SecurityModel newSecurityData = new SecurityModel();
        newSecurityData.setName(name);

        return new PostNewSecurityRequest()
                .setPayload(newSecurityData)
                .sendRequest()
                .assertStatusCode()
                .getResponseModel();
    }

    public SecurityModel getSecurityByName(String name) {
        return getSecurities().stream()
                .filter(security -> security.getName().equals(name))
                .findFirst()
                .orElseThrow(() ->
                        new PendingException("Security '" + name + "' has not been found!"));
    }
}
