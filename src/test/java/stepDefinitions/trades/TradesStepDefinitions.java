package stepDefinitions.trades;

import actions.OrdersActions;
import actions.SecuritiesActions;
import actions.TradesActions;
import actions.UsersActions;
import assertions.OrdersAssertions;
import assertions.TradesAssertions;
import factories.OrdersFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.orders.OrderModel;
import models.trades.TradeModel;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TradesStepDefinitions {
    private final UsersActions usersActions = UsersActions.getUsersAction();
    private final SecuritiesActions securitiesActions = SecuritiesActions.getSecuritiesActions();
    private final OrdersActions ordersActions = OrdersActions.getOrdersActions();
    private final TradesActions tradesActions = TradesActions.getTradesActions();

    private final Map<String, OrderModel> placedOrders = new HashMap<>();

    private static final String DUMMY_PASSWORD = "pass123";

    @Given("Security {} is added to the system")
    public void securityIsPresentInTheSystem(String securityName) {
        securitiesActions.createSecurity(securityName);
    }

    @Given("User {} has signed up")
    public void userHasSignedUp(String username) {
        usersActions.createUser(username, DUMMY_PASSWORD);
    }

    @When("User {} places {} order for security {} with price {} and quantity {}")
    public void userPlacesOrderWithPriceAndQuantity(String username, String orderType, String securitySymbol, String price, String quantity) {
        BigDecimal numPrice = new BigDecimal(price);
        BigDecimal numQuantity = new BigDecimal(quantity);

        OrderModel newOrder = OrdersFactory.buildOrder(
                username, securitySymbol, orderType, numPrice, numQuantity);

        placedOrders.put(orderType, ordersActions.placeOrder(newOrder));
    }

    @Then("Trade should appear with price {} and quantity {} and orders be fulfilled")
    public void tradeShouldAppearWithPriceAndQuantity(String price, String quantity) {
        UUID orderBuyerId = placedOrders.get("BUY").getId();
        UUID orderSellerId = placedOrders.get("SELL").getId();
        BigDecimal numPrice = new BigDecimal(price);
        BigDecimal numQuantity = new BigDecimal(quantity);

        TradeModel actualTrade = tradesActions.getTradeByBuyerAndSellerIds(orderBuyerId, orderSellerId);

        new TradesAssertions().verifyTrade(orderBuyerId, orderSellerId, numPrice, numQuantity, actualTrade);
        new OrdersAssertions().assertOrderIsFulfilled(ordersActions.getOrder(orderBuyerId));
        new OrdersAssertions().assertOrderIsFulfilled(ordersActions.getOrder(orderSellerId));
    }

    @Then("Trade should not happen")
    public void tradeShouldNotHappen() {
        UUID orderBuyerId = placedOrders.get("BUY").getId();
        UUID orderSellerId = placedOrders.get("SELL").getId();

        tradesActions.getFailedTradeByBuyAndSellOrderIds(orderBuyerId, orderSellerId);
    }
}
