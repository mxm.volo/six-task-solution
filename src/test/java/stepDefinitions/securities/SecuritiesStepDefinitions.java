package stepDefinitions.securities;

import actions.SecuritiesActions;
import assertions.SecuritiesAssertions;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.securities.SecurityModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

import static actions.SecuritiesActions.getSecuritiesActions;

public class SecuritiesStepDefinitions {

    private final Logger logger = LoggerFactory.getLogger(SecuritiesStepDefinitions.class);
    private final SecuritiesActions securitiesActions = getSecuritiesActions();

    private List<SecurityModel> expectedSecurities;
    private List<SecurityModel> actualSecurities;

    @Given("New securities are added to the system")
    public void securityWithNameAddedToTheSystem(List<String> securities) {
        expectedSecurities = securities.stream()
                .map(securitiesActions::createSecurity)
                .collect(Collectors.toList());
        logger.info("Added securities");
    }

    @When("List of securities is requested")
    public void listOfSecuritiesIsRequested() {
        actualSecurities = securitiesActions.getSecurities();
        logger.info("Received list of securities");
    }

    @Then("All added securities are returned")
    public void allAddedSecuritiesAreReturned() {
        new SecuritiesAssertions().assertSecuritiesArePresent(actualSecurities, expectedSecurities);
    }
}
