package stepDefinitions.common;

import io.cucumber.java.BeforeAll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import requests.HealthCheckRequest;

public class HooksStepDefinitions {

    private static final Logger logger = LoggerFactory.getLogger("HealthCheck");

    @BeforeAll
    public static void serviceHealthCheck() {
        logger.info("Service health check..");
        new HealthCheckRequest()
                .sendRequest()
                .assertStatusCode();
    }
}
