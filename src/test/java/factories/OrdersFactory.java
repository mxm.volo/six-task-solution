package factories;

import actions.SecuritiesActions;
import actions.UsersActions;
import lombok.experimental.UtilityClass;
import models.orders.OrderModel;
import models.securities.SecurityModel;
import models.users.UserModel;

import java.math.BigDecimal;

import static actions.SecuritiesActions.getSecuritiesActions;
import static actions.UsersActions.getUsersAction;

@UtilityClass
public class OrdersFactory {

    private final UsersActions userActions = getUsersAction();
    private final SecuritiesActions securitiesActions = getSecuritiesActions();

    public OrderModel buildOrder(String username, String name, String type, BigDecimal price, BigDecimal quantity) {
        UserModel user = userActions.getUserByName(username);
        SecurityModel security = securitiesActions.getSecurityByName(name);

        return OrderModel
                .builder()
                .userId(user.getId())
                .securityId(security.getId())
                .type(type)
                .price(price)
                .quantity(quantity)
                .build();
    }
}
