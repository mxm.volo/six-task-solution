package assertions;

import junit.framework.AssertionFailedError;
import models.securities.SecurityModel;

import java.util.List;

public class SecuritiesAssertions {
    public void assertSecuritiesArePresent(List<SecurityModel> actualSecurities, List<SecurityModel> expectedSecurities) {
        expectedSecurities.forEach(expectedSecurity -> {
            boolean foundMatch = actualSecurities.stream().anyMatch(actualSecurity ->

                    actualSecurity.getId().equals(expectedSecurity.getId()) &&
                            actualSecurity.getName().equals(expectedSecurity.getName()));

            if(!foundMatch) {
                throw new AssertionFailedError("Security '" + expectedSecurity.getName() + "' not found!");
            }
        });
    }
}
