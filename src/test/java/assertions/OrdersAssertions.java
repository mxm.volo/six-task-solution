package assertions;

import models.orders.OrderModel;
import org.assertj.core.api.Assertions;

public class OrdersAssertions {

    public void assertOrderIsFulfilled(OrderModel order) {
        Assertions.assertThat(order.getFulfilled()).isTrue();
    }
}
