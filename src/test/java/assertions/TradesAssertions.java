package assertions;

import models.trades.TradeModel;
import org.assertj.core.api.Assertions;

import java.math.BigDecimal;
import java.util.UUID;

public class TradesAssertions {

    public void verifyTrade(UUID orderBuyerId, UUID orderSellerId, BigDecimal price, BigDecimal quantity, TradeModel actualTrade) {
        org.junit.jupiter.api.Assertions.assertAll(
                () -> Assertions.assertThat(orderBuyerId).isEqualTo(actualTrade.getOrderBuyId()),
                () -> Assertions.assertThat(orderSellerId).isEqualTo(actualTrade.getOrderSellId()),
                () -> Assertions.assertThat(price).isEqualByComparingTo(actualTrade.getPrice()),
                () -> Assertions.assertThat(quantity).isEqualByComparingTo(actualTrade.getQuantity())
        );
    }
}
