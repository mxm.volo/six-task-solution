package requests.securities;

import endpoints.securities.SecuritiesEndpoint;
import io.restassured.common.mapper.TypeRef;
import models.securities.SecurityModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.List;

public class GetAllSecuritiesRequest extends SecuritiesEndpoint<GetAllSecuritiesRequest, List<SecurityModel>> {
    @Override
    protected Type getModel() {
        return new TypeRef<List<SecurityModel>>() {}.getType();
    }

    @Override
    public GetAllSecuritiesRequest sendRequest() {
        setResponse(getService().getSecurities());
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
