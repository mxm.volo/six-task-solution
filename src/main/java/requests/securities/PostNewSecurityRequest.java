package requests.securities;

import endpoints.securities.SecuritiesEndpoint;
import lombok.Setter;
import lombok.experimental.Accessors;
import models.securities.SecurityModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class PostNewSecurityRequest extends SecuritiesEndpoint<PostNewSecurityRequest, SecurityModel> {
    @Setter
    @Accessors(chain = true)
    private SecurityModel payload;

    @Override
    protected Type getModel() {
        return SecurityModel.class;
    }

    @Override
    public PostNewSecurityRequest sendRequest() {
        setResponse(getService().createSecurity(payload));
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_CREATED;
    }
}
