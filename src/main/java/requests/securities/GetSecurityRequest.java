package requests.securities;

import endpoints.securities.SecuritiesEndpoint;
import lombok.Setter;
import lombok.experimental.Accessors;
import models.securities.SecurityModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.UUID;

public class GetSecurityRequest extends SecuritiesEndpoint<GetSecurityRequest, SecurityModel> {
    @Setter
    @Accessors(chain = true)
    private UUID securityId;

    @Override
    protected Type getModel() {
        return SecurityModel.class;
    }

    @Override
    public GetSecurityRequest sendRequest() {
        setResponse(getService().getSecurity(securityId));
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
