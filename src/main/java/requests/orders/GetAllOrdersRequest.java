package requests.orders;

import endpoints.orders.OrdersEndpoint;
import io.restassured.common.mapper.TypeRef;
import models.orders.OrderModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.List;

public class GetAllOrdersRequest extends OrdersEndpoint<GetAllOrdersRequest, List<OrderModel>> {
    @Override
    protected Type getModel() {
        return new TypeRef<List<OrderModel>>() {}.getType();
    }

    @Override
    public GetAllOrdersRequest sendRequest() {
        setResponse(getService().getOrders());
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
