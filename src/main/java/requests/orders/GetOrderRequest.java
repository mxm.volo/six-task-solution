package requests.orders;

import endpoints.orders.OrdersEndpoint;
import lombok.Setter;
import lombok.experimental.Accessors;
import models.orders.OrderModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.UUID;

public class GetOrderRequest extends OrdersEndpoint<GetOrderRequest, OrderModel> {
    @Setter
    @Accessors(chain = true)
    private UUID orderId;

    @Override
    protected Type getModel() {
        return OrderModel.class;
    }

    @Override
    public GetOrderRequest sendRequest() {
        setResponse(getService().getOrder(orderId));
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
