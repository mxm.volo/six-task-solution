package requests.orders;

import endpoints.orders.OrdersEndpoint;
import lombok.Setter;
import lombok.experimental.Accessors;
import models.orders.OrderModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class PostNewOrderRequest extends OrdersEndpoint<PostNewOrderRequest, OrderModel> {
    @Setter
    @Accessors(chain = true)
    private OrderModel payload;

    @Override
    protected Type getModel() {
        return OrderModel.class;
    }

    @Override
    public PostNewOrderRequest sendRequest() {
        setResponse(getService().placeOrder(payload));
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_CREATED;
    }
}
