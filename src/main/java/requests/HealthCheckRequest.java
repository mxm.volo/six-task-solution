package requests;

import endpoints.HealthCheckEndpoint;
import models.EmptyResponse;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class HealthCheckRequest extends HealthCheckEndpoint<HealthCheckRequest, EmptyResponse> {
    @Override
    public HealthCheckRequest sendRequest() {
        setResponse(getService().reachOut());
        return this;
    }

    @Override
    protected Type getModel() {
        return EmptyResponse.class;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
