package requests.trades;

import endpoints.trades.TradesEndpoint;
import lombok.Setter;
import lombok.experimental.Accessors;
import models.trades.TradeModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.UUID;

public class GetSpecificTradeRequest extends TradesEndpoint<GetSpecificTradeRequest, TradeModel> {
    @Setter
    @Accessors(chain = true)
    private UUID orderBuyerId;

    @Setter
    @Accessors(chain = true)
    private UUID orderSellerID;

    public GetSpecificTradeRequest setBuyerAndSellerIds(UUID orderBuyerId, UUID orderSellerID) {
        this.orderBuyerId = orderBuyerId;
        this.orderSellerID = orderSellerID;
        return this;
    }

    @Override
    protected Type getModel() {
        return TradeModel.class;
    }

    @Override
    public GetSpecificTradeRequest sendRequest() {
        setResponse(getService().getSpecificTrade(orderBuyerId, orderSellerID));
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
