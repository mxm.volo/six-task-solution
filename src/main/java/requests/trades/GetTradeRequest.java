package requests.trades;

import endpoints.trades.TradesEndpoint;
import lombok.Setter;
import lombok.experimental.Accessors;
import models.trades.TradeModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.UUID;

public class GetTradeRequest extends TradesEndpoint<GetTradeRequest, TradeModel> {
    @Setter
    @Accessors(chain = true)
    private UUID tradeId;

    @Override
    protected Type getModel() {
        return TradeModel.class;
    }

    @Override
    public GetTradeRequest sendRequest() {
        setResponse(getService().getTrade(tradeId));
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
