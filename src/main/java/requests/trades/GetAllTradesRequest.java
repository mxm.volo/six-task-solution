package requests.trades;

import endpoints.trades.TradesEndpoint;
import io.restassured.common.mapper.TypeRef;
import models.trades.TradeModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.List;

public class GetAllTradesRequest extends TradesEndpoint<GetAllTradesRequest, List<TradeModel>> {

    @Override
    protected Type getModel() {
        return new TypeRef<List<TradeModel>>() {}.getType();
    }

    @Override
    public GetAllTradesRequest sendRequest() {
        setResponse(getService().getTrades());
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
