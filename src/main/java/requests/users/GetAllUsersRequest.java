package requests.users;

import endpoints.users.UsersEndpoint;
import io.restassured.common.mapper.TypeRef;
import models.users.UserModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.List;

public class GetAllUsersRequest extends UsersEndpoint<GetAllUsersRequest, List<UserModel>> {
    @Override
    public GetAllUsersRequest sendRequest() {
        setResponse(getService().getUsers());
        return this;
    }

    @Override
    protected Type getModel() {
        return new TypeRef<List<UserModel>>() {}.getType();
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
