package requests.users;

import endpoints.users.UsersEndpoint;
import lombok.Setter;
import lombok.experimental.Accessors;
import models.users.UserModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.UUID;

public class GetUserRequest extends UsersEndpoint<GetUserRequest, UserModel> {
    @Setter
    @Accessors(chain = true)
    private UUID userId;

    @Override
    public GetUserRequest sendRequest() {
        setResponse(getService().getUser(userId));
        return this;
    }

    @Override
    protected Type getModel() {
        return UserModel.class;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
