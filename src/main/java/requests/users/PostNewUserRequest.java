package requests.users;

import endpoints.users.UsersEndpoint;
import lombok.Setter;
import lombok.experimental.Accessors;
import models.users.UserModel;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class PostNewUserRequest extends UsersEndpoint<PostNewUserRequest, UserModel> {
    @Setter
    @Accessors(chain = true)
    private UserModel payload;

    @Override
    public PostNewUserRequest sendRequest() {
        setResponse(getService().createUser(payload));
        return this;
    }

    @Override
    protected Type getModel() {
        return UserModel.class;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_CREATED;
    }
}
