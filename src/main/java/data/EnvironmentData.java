package data;

import lombok.experimental.UtilityClass;

import java.util.Map;
import java.util.Optional;

import static utils.YamlUtils.readEnvironmentYaml;

@UtilityClass
public class EnvironmentData {
    private final String TEST_ENVIRONMENT_ENV = "TEST_ENVIRONMENT";

    public final String HOST;
    public final String PATH;
    public final String BASE_URL;

    static {
        String environment = Optional.ofNullable(System.getenv(TEST_ENVIRONMENT_ENV))
                .orElse("local");

        Map<String, String> environmentData = readEnvironmentYaml(environment);

        HOST = environmentData.get("host");
        PATH = environmentData.get("path");
        BASE_URL = HOST + PATH;
    }
}
