package services;

import data.EnvironmentData;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

public abstract class BaseService {

    protected RequestSpecification spec;

    protected abstract String getResourcePath();

    protected RequestSpecification getServiceSpecification() {
        return getInitialSpecification().basePath(getResourcePath());
    }

    private RequestSpecification getInitialSpecification() {
        if(spec == null) {
            spec = SerenityRest.rest()
                    .baseUri(EnvironmentData.BASE_URL)
                    .contentType(ContentType.JSON)
                    .log()
                    .uri()
                    .log()
                    .method()
                    .response()
                    .log()
                    .ifError()
                    .log()
                    .status()
                    .request();
        }
        return spec;
    }
}
