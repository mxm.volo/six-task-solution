package services.securities;

import io.restassured.response.Response;
import models.securities.SecurityModel;
import services.BaseService;

import java.util.UUID;

public class SecuritiesService extends BaseService {
    @Override
    protected String getResourcePath() {
        return "/securities";
    }

    public Response getSecurities() {
        return getServiceSpecification()
                .get();
    }

    public Response getSecurity(UUID id) {
        return getServiceSpecification()
                .get(id.toString());
    }

    public Response createSecurity(SecurityModel payload) {
        return getServiceSpecification()
                .body(payload)
                .post();
    }
}
