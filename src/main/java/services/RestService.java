package services;

public interface RestService<T> {
    T getService();
}
