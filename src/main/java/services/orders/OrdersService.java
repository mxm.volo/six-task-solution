package services.orders;

import io.restassured.response.Response;
import models.orders.OrderModel;
import services.BaseService;

import java.util.UUID;

public class OrdersService extends BaseService {
    @Override
    protected String getResourcePath() {
        return "/orders";
    }

    public Response getOrders() {
        return getServiceSpecification()
                .get();
    }

    public Response getOrder(UUID id) {
        return getServiceSpecification()
                .get(id.toString());
    }

    public Response placeOrder(OrderModel payload) {
        return getServiceSpecification()
                .body(payload)
                .post();
    }
}
