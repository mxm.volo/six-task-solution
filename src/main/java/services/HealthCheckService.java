package services;

import data.EnvironmentData;
import io.restassured.response.Response;

public class HealthCheckService extends BaseService {
    @Override
    protected String getResourcePath() {
        return "/";
    }

    public Response reachOut() {
        return getServiceSpecification()
                .baseUri(EnvironmentData.HOST)
                .get();
    }
}
