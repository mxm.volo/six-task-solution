package services.trades;

import io.restassured.response.Response;
import services.BaseService;

import java.util.UUID;

public class TradesService extends BaseService {
    @Override
    protected String getResourcePath() {
        return "/trades";
    }

    public Response getTrades() {
        return getServiceSpecification()
                .get();
    }

    public Response getTrade(UUID id) {
        return getServiceSpecification()
                .get(id.toString());
    }

    public Response getSpecificTrade(UUID buyerId, UUID sellerID) {
        return getServiceSpecification()
                .get("orderBuyId/" + buyerId + "/orderSellId/" + sellerID);
    }
}
