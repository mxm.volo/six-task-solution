package services.users;
import io.restassured.response.Response;
import models.users.UserModel;
import services.BaseService;

import java.util.UUID;

public class UsersService extends BaseService {

    @Override
    protected String getResourcePath() {
        return "/users";
    }

    public Response getUsers() {
        return getServiceSpecification()
                .get();
    }

    public Response getUser(UUID id) {
        return getServiceSpecification()
                .get(id.toString());
    }

    public Response createUser(UserModel payload) {
        return getServiceSpecification()
                .body(payload)
                .post();
    }
}
