package endpoints.trades;

import endpoints.BaseEndpoint;
import services.RestService;
import services.trades.TradesService;

public abstract class TradesEndpoint<R, M> extends BaseEndpoint<R, M> implements RestService<TradesService> {
    @Override
    public TradesService getService() {
        return new TradesService();
    }
}
