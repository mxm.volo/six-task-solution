package endpoints.securities;

import endpoints.BaseEndpoint;
import services.RestService;
import services.securities.SecuritiesService;

public abstract class SecuritiesEndpoint<R, M> extends BaseEndpoint<R, M> implements RestService<SecuritiesService> {
    @Override
    public SecuritiesService getService() {
        return new SecuritiesService();
    }
}
