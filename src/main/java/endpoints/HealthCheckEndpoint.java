package endpoints;

import services.HealthCheckService;
import services.RestService;

public abstract class HealthCheckEndpoint<R, M> extends BaseEndpoint<R, M> implements RestService<HealthCheckService> {
    @Override
    public HealthCheckService getService() {
        return new HealthCheckService();
    }
}
