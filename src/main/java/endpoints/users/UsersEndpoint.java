package endpoints.users;

import endpoints.BaseEndpoint;
import services.RestService;
import services.users.UsersService;

public abstract class UsersEndpoint<R, M> extends BaseEndpoint<R, M> implements RestService<UsersService> {
    @Override
    public UsersService getService() {
        return new UsersService();
    }
}
