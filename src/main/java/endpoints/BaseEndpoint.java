package endpoints;

import io.restassured.response.Response;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import models.ErrorModel;
import org.assertj.core.api.Assertions;

import java.lang.reflect.Type;

public abstract class BaseEndpoint<R, M> {
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PROTECTED)
    protected Response response;

    private void assertResponse() {
        Assertions.assertThat(getResponse()).isNotNull();
    }

    public abstract R sendRequest();

    protected abstract Type getModel();

    protected abstract int getSuccessStatusCode();

    public M getResponseModel() {
        assertResponse();
        return getResponse().as(getModel());
    }

    public ErrorModel getErrorModel() {
        assertResponse();
        return getResponse().as(ErrorModel.class);
    }

    public R assertStatusCode(int expectedStatusCode) {
        assertResponse();
        Assertions.assertThat(getResponse().getStatusCode()).isEqualTo(expectedStatusCode);
        return (R) this;
    }

    public R assertStatusCode() {
        assertStatusCode(getSuccessStatusCode());
        return (R) this;
    }
}
