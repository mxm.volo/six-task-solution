package endpoints.orders;

import endpoints.BaseEndpoint;
import services.RestService;
import services.orders.OrdersService;

public abstract class OrdersEndpoint<R, M> extends BaseEndpoint<R, M> implements RestService<OrdersService> {
    @Override
    public OrdersService getService() {
        return new OrdersService();
    }
}
