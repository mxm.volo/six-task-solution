package models.orders;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderModel {
    private UUID id;
    private UUID userId;
    private UUID securityId;
    private String type;
    private BigDecimal price;
    private BigDecimal quantity;
    @Builder.Default
    private Boolean fulfilled = false;
}