package models;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class EmptyResponse {
    // intended to represent empty response
}
