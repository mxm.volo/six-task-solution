package models.trades;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradeModel {
    private UUID id;
    private UUID orderSellId;
    private UUID orderBuyId;
    private BigDecimal price;
    private BigDecimal quantity;
}
