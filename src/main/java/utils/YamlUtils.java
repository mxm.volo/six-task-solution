package utils;

import data.EnvironmentData;
import lombok.experimental.UtilityClass;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.Map;

@UtilityClass
public class YamlUtils {

    public static Map<String, String> readEnvironmentYaml(String environment) {
        Yaml yaml = new Yaml();
        InputStream inputStream = EnvironmentData.class.getClassLoader()
                .getResourceAsStream("environments/" + environment.toLowerCase() + ".yaml");

        return yaml.load(inputStream);
    }
}
