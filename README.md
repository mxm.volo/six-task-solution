# six-task-solution

Created by Maxim Volosenco for SIX

## Getting started

This project has been created to solve coding challenge provided by SIX-Group. Please follow the instruction below to build and run the testing project.

### Required software

* [ ] [JDK 18](https://www.oracle.com/java/technologies/downloads/#JDK18)
* [ ] [Maven 3.8.1](https://maven.apache.org/download.cgi) or later

### Prerequisites

Please before triggering the test run, **make sure that the trading app is running on your local on `localhost:8080`** or add a configuration file with desired environment data.

## How to build the project

In order to build the solution, please clone the project from the git repository to your local machine:

```shell
git clone https://gitlab.com/mxm.volo/six-task-solution.git
```

After that, enter the solutions' directory and trigger the maven build with the following commands:
```shell
cd six-task-solution
mvn -U clean install -DskipTests
```

The last command will update snapshots and download the dependencies from the `pom.xml` file for this project and store them in the `.m2` folder.

## How to execute tests

### On local

To start the test run, execute the following maven command:
```shell
TEST_ENVIRONMENT=local mvn clean verify
```
**Environment change support was added for possible extensions in the future.*

By default, tests expect the trading app to be running on `http://localhost:8080`. If you would like to change port, please go to `src/test/resources/environment/local.yaml` and change port at the `host` property.
New environment can be added by placing `<TEST_ENVIRONMENT>.yaml` to `src/test/resources/environment/` where `TEST_ENVIRONMENT` is the name of the environment and setting `TEST_ENVIRONMENT` environment variable to that environment name.

### Using Docker

To trigger test run in a docker container, please execute the following command being at the root folder of the project:
```shell
docker build -t six-task-solution --progress plain .
```

This command will build the image with verbose logs and defined image tag.
Execute the following command to run the container: 
```shell
docker run --env TEST_ENVIRONMENT=container -v /<ABSOLUT_PATH_TO_PROJECT>/container-target:/app/target/ six-image-solution:latest
```
This command will spin up a container with the test project being executed inside. It will save `/target` folder to the location on the host machine specified by `<ABSOLUT_PATH_TO_PROJECT>` in `container-target` folder, for example `/Users/user/Documents/six-task-solution/container-target`.
`--env TEST_ENVIRONMENT=container` instructs the container to allow tests to reach the trading app running on the host machine on `localhost:8080`.

## How to check reports

- Serenity report can be found after each test run at `target/site/serenity/index.html`.
- If test run was executed in docker container, report will be stored at the location specified by the user at `docker run` command.
